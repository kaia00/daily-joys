package com.sda.finalproject.dailyjoys.service.impl;


import com.sda.finalproject.dailyjoys.dto.UserDTO;
import com.sda.finalproject.dailyjoys.exceptions.IdNotFoundException;
import com.sda.finalproject.dailyjoys.exceptions.UserNotFoundException;
import com.sda.finalproject.dailyjoys.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;

import static org.junit.Assert.assertEquals;


@SpringBootTest
@RunWith(SpringRunner.class)
@Sql(scripts = {"/insert-data.sql"})
public class UserServiceImplTest {


    @Autowired
    private UserService userService;

    @Test
    public void testFindByUsernameReturnUser() throws UserNotFoundException {

        UserDTO user = userService.findByUsername("kaia");

        assertEquals("kaia", user.getUsername());

    }

    @Test
    public void testUpdateUserReturnUser() throws IdNotFoundException, UserNotFoundException {

        UserDTO user = userService.findByUsername("kaia");
        user.setFirstName("Test");

        UserDTO updatedUser = userService.update(user.getId(), user);

        assertEquals("Test", updatedUser.getFirstName());
    }

    @Test(expected = UserNotFoundException.class)
    public void findByUsernameExpectUserNotFoundException() throws UserNotFoundException {

     userService.findByUsername("random");



    }


}
