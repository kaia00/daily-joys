package com.sda.finalproject.dailyjoys.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sda.finalproject.dailyjoys.dto.UserDTO;
import com.sda.finalproject.dailyjoys.exceptions.UserNotFoundException;
import com.sda.finalproject.dailyjoys.service.UserService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
public class UserControllerTest {

    @MockBean
    UserController userController;

    @MockBean
    UserService userService;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;


    @Test
    public void testFindByUsernameReturnHttpStatusOk() throws UserNotFoundException, Exception {
        UserDTO user = new UserDTO();
        user.setUsername("kaia");

        ResponseEntity responseEntity = new ResponseEntity(user, HttpStatus.OK);

        given(userController.findByUsername(user.getUsername())).willReturn(responseEntity);

        mockMvc.perform(get("/users/search?username=kaia"))
                .andExpect(status().isOk());
    }

    @Test
    public void testAddUserReturnHttpStatusCreated() throws Exception {
        UserDTO user = new UserDTO();
        user.setUsername("kaia");

        ResponseEntity responseEntity = new ResponseEntity(user, HttpStatus.CREATED);

        given(userController.addUser(user)).willReturn(responseEntity);

        String json = mapper.writeValueAsString(user);

        mockMvc.perform(post("/users/add")
                .contentType((MediaType.APPLICATION_JSON))
                .content(json)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

}

