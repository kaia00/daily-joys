package com.sda.finalproject.dailyjoys.service;

import com.sda.finalproject.dailyjoys.exceptions.RequestNotFoundException;
import com.sda.finalproject.dailyjoys.exceptions.UserNotFoundException;
import com.sda.finalproject.dailyjoys.dto.RequestDTO;

public interface RequestService {

    RequestDTO sendFriendRequest(String userName) throws UserNotFoundException;

    RequestDTO acceptFriendRequest(Long id) throws RequestNotFoundException;

    RequestDTO rejectFriendRequest(Long id) throws RequestNotFoundException;

}
