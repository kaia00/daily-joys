package com.sda.finalproject.dailyjoys.service.impl;

import com.sda.finalproject.dailyjoys.dto.PostDTO;
import com.sda.finalproject.dailyjoys.dto.UserDTO;
import com.sda.finalproject.dailyjoys.exceptions.IdNotFoundException;
import com.sda.finalproject.dailyjoys.exceptions.PostNotFoundException;
import com.sda.finalproject.dailyjoys.model.Post;
import com.sda.finalproject.dailyjoys.model.User;
import com.sda.finalproject.dailyjoys.repository.PostRepository;
import com.sda.finalproject.dailyjoys.repository.UserRepository;
import com.sda.finalproject.dailyjoys.service.PostService;
import com.sda.finalproject.dailyjoys.util.DTOToPost;
import com.sda.finalproject.dailyjoys.util.PostToDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

@Service
public class PostServiceImpl implements PostService {


    private final PostRepository postRepository;
    private final UserRepository userRepository;

    @Autowired
    public PostServiceImpl(PostRepository postRepository, UserRepository userRepository) {
        this.postRepository = postRepository;
        this.userRepository = userRepository;
    }

    @Override
    public PostDTO add(PostDTO postDTO) {
        Post post = new Post();
        post.setContent(postDTO.getContent());
        post.setDate(LocalDateTime.now());
        post.setUserId(postDTO.getUserId());  // todo check if user exists

        User user = userRepository.findById(postDTO.getUserId()).get();

        Set<Post> posts = user.getPosts();

        Post postToSave = postRepository.save(post);
        posts.add(postToSave);
        user.setPosts(posts);

        userRepository.save(user);

        return PostToDTO.convert(postToSave);

    }

    @Override
    public String delete(Long id) throws PostNotFoundException {
        if (postRepository.findById(id).isPresent()) {
            Post postToDelete = postRepository.findById(id).get();
            postRepository.delete(postToDelete);
            return "Post deleted!";
        }
        throw new PostNotFoundException("No post with that ID was found");
    }

    @Override
    public PostDTO update(Long id, PostDTO post) throws PostNotFoundException {
        if (postRepository.findById(id).isPresent()) {
            PostDTO post1 = PostToDTO.convert(postRepository.findById(id).get());
            post1.setContent(post.getContent());
            Post savedPost = postRepository.save(DTOToPost.convert(post1));
            return PostToDTO.convert(savedPost);
        }
        throw new PostNotFoundException("No post with that ID was found");

    }

    @Override
    public PostDTO findById(Long id) throws PostNotFoundException {
        if (postRepository.findById(id).isPresent()) {
            return PostToDTO.convert(postRepository.findById(id).get());
        }
        throw new PostNotFoundException("No post with that ID was found");

    }

    @Override
    public List<PostDTO> showAllPosts() throws IdNotFoundException {
        List<PostDTO> posts = new ArrayList<>();
        for (Post post : postRepository.findPostsByUserId(1L)) {  //todo show all posts for logged in user!
            posts.add(PostToDTO.convert(post));
        }
        return posts;
        //throw new IdNotFoundException("No user with that ID was found");

    }


}
