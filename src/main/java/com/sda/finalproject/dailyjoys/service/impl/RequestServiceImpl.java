package com.sda.finalproject.dailyjoys.service.impl;


import com.sda.finalproject.dailyjoys.exceptions.RequestNotFoundException;
import com.sda.finalproject.dailyjoys.exceptions.UserNotFoundException;
import com.sda.finalproject.dailyjoys.model.Friend;
import com.sda.finalproject.dailyjoys.dto.RequestDTO;
import com.sda.finalproject.dailyjoys.model.RequestStatus;
import com.sda.finalproject.dailyjoys.model.User;
import com.sda.finalproject.dailyjoys.repository.FriendRepository;
import com.sda.finalproject.dailyjoys.repository.RequestRepository;
import com.sda.finalproject.dailyjoys.repository.UserRepository;
import com.sda.finalproject.dailyjoys.service.RequestService;
import com.sda.finalproject.dailyjoys.util.DTOToRequest;
import com.sda.finalproject.dailyjoys.util.RequestToDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.Set;


@Service
public class RequestServiceImpl implements RequestService {

    private final RequestRepository requestRepository;
    private final UserRepository userRepository;
    private final FriendRepository friendRepository;

    @Autowired
    public RequestServiceImpl(RequestRepository requestRepository, UserRepository userRepository, FriendRepository friendRepository) {
        this.requestRepository = requestRepository;
        this.userRepository = userRepository;
        this.friendRepository = friendRepository;
    }

    @Override
    public RequestDTO sendFriendRequest(String username) throws UserNotFoundException {

        if (userRepository.findByUsername(username) != null) {
            RequestDTO request = new RequestDTO();

            request.setDate(LocalDateTime.now());
            request.setRequestStatus(RequestStatus.PENDING);

            User requester = userRepository.findByUsername("brit");
            userRepository.save(requester);
            request.setRequesterId(requester.getId());

            User receiver = userRepository.findByUsername(username);
            userRepository.save(receiver);
            request.setReceiverId(receiver.getId());

            // todo set requester as logged in user?

            return RequestToDTO.convert(requestRepository.save(DTOToRequest.convert(request)));

        }
        throw new UserNotFoundException("We couldn't find a user with that username");

    }

    @Override
    public RequestDTO acceptFriendRequest(Long id) throws RequestNotFoundException {

        if (requestRepository.findById(id).isPresent()) {

            Long receiverId = requestRepository.findById(id).get().getReceiverId();

            RequestDTO acceptedRequest = new RequestDTO();
            acceptedRequest.setRequestStatus(RequestStatus.ACCEPTED);
            acceptedRequest.setDate(LocalDateTime.now());
            acceptedRequest.setId(id);

            User receiver = userRepository.findById(receiverId).get();
            User requester = userRepository.findById(1L).get();

            Friend friendReceiver = new Friend();
            friendReceiver.setUserId(receiverId);
            Set<Friend> requesterFriends = requester.getFriends();
            requesterFriends.add(friendReceiver);

            friendRepository.save(friendReceiver);
            userRepository.save(requester);


            Friend friendRequester = new Friend();
            friendRequester.setUserId(1L);
            Set<Friend> receiverFriends = receiver.getFriends();
            receiverFriends.add(friendRequester);

            friendRepository.save(friendRequester);
            userRepository.save(receiver);

            acceptedRequest.setRequesterId(requestRepository.findById(id).get().getRequesterId());
            acceptedRequest.setReceiverId(receiverId);

            return RequestToDTO.convert(requestRepository.save(DTOToRequest.convert(acceptedRequest)));

        }
        throw new RequestNotFoundException("No request with that ID was found!");
    }

    @Override
    public RequestDTO rejectFriendRequest(Long id) throws RequestNotFoundException {

        if (requestRepository.findById(id).isPresent()) {

            RequestDTO rejectedRequest = new RequestDTO();
            rejectedRequest.setRequestStatus(RequestStatus.REJECTED);
            rejectedRequest.setDate(LocalDateTime.now());
            rejectedRequest.setId(id);

//            User receiver = requestRepository.findById(id).get().getReceiver();
//            User requester = requestRepository.findById(id).get().getRequester();
//
//            userRepository.save(receiver);
//            userRepository.save(requester);

            rejectedRequest.setReceiverId(requestRepository.findById(id).get().getReceiverId());
            rejectedRequest.setRequesterId(requestRepository.findById(id).get().getRequesterId());

            return RequestToDTO.convert(requestRepository.save(DTOToRequest.convert(rejectedRequest)));

        }
        throw new RequestNotFoundException("No request with that ID was found!");

    }


}
