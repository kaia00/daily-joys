package com.sda.finalproject.dailyjoys.service.impl;

import com.sda.finalproject.dailyjoys.dto.UserDTO;
import com.sda.finalproject.dailyjoys.exceptions.IdNotFoundException;
import com.sda.finalproject.dailyjoys.exceptions.UserNotFoundException;
import com.sda.finalproject.dailyjoys.model.User;
import com.sda.finalproject.dailyjoys.repository.UserRepository;
import com.sda.finalproject.dailyjoys.service.UserService;
import com.sda.finalproject.dailyjoys.util.DTOToUser;
import com.sda.finalproject.dailyjoys.util.UserToDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public UserDTO add(UserDTO user) {

        return UserToDTO.convert(userRepository.save(DTOToUser.convert(user)));
    }

    @Override
    public UserDTO findById(Long id) throws IdNotFoundException {
        if (userRepository.findById(id).isPresent()) {
            return UserToDTO.convert(userRepository.findById(id).get());
        }
        throw new IdNotFoundException("No user with that id was found!");
    }

    @Override
    public UserDTO findByUsername(String username) throws UserNotFoundException {

        if (userRepository.findByUsername(username) != null) {
            return UserToDTO.convert(userRepository.findByUsername(username));
        }
        throw new UserNotFoundException("We couldn't find a user with that username");


    }

    @Override
    public String delete(Long id) throws IdNotFoundException {
        if (userRepository.findById(id).isPresent()) {
            User userToDelete = userRepository.findById(id).get();
            userRepository.delete(userToDelete);
            return "User deleted!";
        }
        throw new IdNotFoundException("No user with that ID was found");
    }

    @Override
    public UserDTO update(Long id, UserDTO user) throws IdNotFoundException {
        if (userRepository.findById(id).isPresent()) {
            UserDTO user1 = UserToDTO.convert(userRepository.findById(id).get());
            user1.setFirstName(user.getFirstName());
            user1.setLastName(user.getLastName());
            user1.setAge(user.getAge());
            user1.setEmail(user.getEmail());
            user1.setPosts(user.getPosts());
            user1.setFriends(user.getFriends());
            return UserToDTO.convert(userRepository.save(DTOToUser.convert(user1)));

        }
        throw new IdNotFoundException("No user with that ID was found!");
    }
}
