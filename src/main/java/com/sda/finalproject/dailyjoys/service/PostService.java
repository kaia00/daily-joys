package com.sda.finalproject.dailyjoys.service;

import com.sda.finalproject.dailyjoys.dto.PostDTO;
import com.sda.finalproject.dailyjoys.exceptions.IdNotFoundException;
import com.sda.finalproject.dailyjoys.exceptions.PostNotFoundException;

import java.util.List;

public interface PostService {

    PostDTO add(PostDTO postDTO);

    String delete(Long id) throws PostNotFoundException;

    PostDTO update(Long id, PostDTO post) throws PostNotFoundException;

    PostDTO findById(Long id) throws PostNotFoundException;

    List<PostDTO> showAllPosts() throws IdNotFoundException;


}
