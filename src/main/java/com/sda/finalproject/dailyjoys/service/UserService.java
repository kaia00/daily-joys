package com.sda.finalproject.dailyjoys.service;

import com.sda.finalproject.dailyjoys.dto.UserDTO;
import com.sda.finalproject.dailyjoys.exceptions.IdNotFoundException;
import com.sda.finalproject.dailyjoys.exceptions.UserNotFoundException;

public interface UserService {

    UserDTO add(UserDTO user);

    UserDTO findById(Long id) throws IdNotFoundException;

    UserDTO findByUsername(String username) throws UserNotFoundException;

    String delete(Long id) throws IdNotFoundException;

    UserDTO update(Long id, UserDTO user) throws IdNotFoundException;
}
