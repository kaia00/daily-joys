package com.sda.finalproject.dailyjoys.exceptions;

public class PostNotFoundException extends Throwable {

    public PostNotFoundException(String message) {
        super(message);
    }
}
