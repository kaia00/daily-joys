package com.sda.finalproject.dailyjoys.exceptions;

public class IdNotFoundException extends Throwable {

    public IdNotFoundException(String message){
        super(message);
    }
}
