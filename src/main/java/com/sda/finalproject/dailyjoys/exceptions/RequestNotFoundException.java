package com.sda.finalproject.dailyjoys.exceptions;

public class RequestNotFoundException extends Throwable {

    public RequestNotFoundException(String message) {
        super(message);
    }
}
