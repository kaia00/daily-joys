package com.sda.finalproject.dailyjoys.util;

import com.sda.finalproject.dailyjoys.dto.UserDTO;
import com.sda.finalproject.dailyjoys.model.User;

public class DTOToUser {

    public static User convert(UserDTO userDTO){

        User user = new User();
        user.setEmail(userDTO.getEmail());
        user.setAge(userDTO.getAge());
        user.setLastName(userDTO.getLastName());
        user.setFirstName(userDTO.getFirstName());
        user.setUsername(userDTO.getUsername());
        user.setFriends(userDTO.getFriends());
        user.setId(userDTO.getId());
        user.setPassword(userDTO.getPassword());
        user.setPosts(userDTO.getPosts());
//        user.setReceivedRequests(userDTO.getReceivedRequests());
//        user.setSentRequests(userDTO.getSentRequests());

        return user;
    }
}
