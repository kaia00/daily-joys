package com.sda.finalproject.dailyjoys.util;

import com.sda.finalproject.dailyjoys.dto.PostDTO;
import com.sda.finalproject.dailyjoys.model.Post;

public class DTOToPost {

    public static Post convert(PostDTO postDTO){

        Post post = new Post();
        post.setContent(postDTO.getContent());
        post.setUserId(postDTO.getUserId());
        post.setDate(postDTO.getDate());
        post.setId(postDTO.getPostId());

        return post;
    }
}
