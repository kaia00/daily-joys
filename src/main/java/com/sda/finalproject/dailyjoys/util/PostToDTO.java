package com.sda.finalproject.dailyjoys.util;

import com.sda.finalproject.dailyjoys.dto.PostDTO;
import com.sda.finalproject.dailyjoys.model.Post;

public class PostToDTO {

    public static PostDTO convert(Post post){

        PostDTO postDTO = new PostDTO();

        postDTO.setContent(post.getContent());
        postDTO.setDate(post.getDate());
        postDTO.setPostId(post.getId());
        postDTO.setUserId(post.getUserId());

        return postDTO;
    }

}
