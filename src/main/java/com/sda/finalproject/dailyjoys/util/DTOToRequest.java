package com.sda.finalproject.dailyjoys.util;

import com.sda.finalproject.dailyjoys.dto.RequestDTO;
import com.sda.finalproject.dailyjoys.model.Request;

public class DTOToRequest {

    public static Request convert(RequestDTO requestDTO){

        Request request = new Request();

        request.setRequestStatus(requestDTO.getRequestStatus());
        request.setDate(requestDTO.getDate());

        request.setReceiverId(requestDTO.getReceiverId());
        request.setRequesterId(requestDTO.getRequesterId());

        request.setId(requestDTO.getId());
      //  request.setReceiverId(requestDTO.getReceiverId());

        return request;
    }
}
