package com.sda.finalproject.dailyjoys.util;

import com.sda.finalproject.dailyjoys.dto.UserDTO;
import com.sda.finalproject.dailyjoys.model.User;

public class UserToDTO {

    public static UserDTO convert(User user) {

        UserDTO userDTO = new UserDTO();

        userDTO.setAge(user.getAge());
        userDTO.setEmail(user.getEmail());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setId(user.getId());
        userDTO.setLastName(user.getLastName());
        userDTO.setPassword(user.getPassword());
        userDTO.setUsername(user.getUsername());
        userDTO.setFriends(user.getFriends());
        userDTO.setPosts(user.getPosts());
//        userDTO.setReceivedRequests(user.getReceivedRequests());
//        userDTO.setSentRequests(user.getSentRequests());


        return userDTO;
    }
}
