package com.sda.finalproject.dailyjoys.util;

import com.sda.finalproject.dailyjoys.dto.RequestDTO;
import com.sda.finalproject.dailyjoys.model.Request;

public class RequestToDTO {

    public static RequestDTO convert(Request request) {

        RequestDTO requestDTO = new RequestDTO();

        requestDTO.setDate(request.getDate());
        requestDTO.setId(request.getId());


        requestDTO.setReceiverId(request.getReceiverId());
        requestDTO.setRequesterId(request.getRequesterId());


        requestDTO.setRequestStatus(request.getRequestStatus());

//        requestDTO.setReceiverId(request.getReceiverId());
//        requestDTO.setRequesterId(request.getRequesterId());

        return requestDTO;
    }
}
