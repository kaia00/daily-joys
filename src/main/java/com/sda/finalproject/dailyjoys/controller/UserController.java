package com.sda.finalproject.dailyjoys.controller;

import com.sda.finalproject.dailyjoys.dto.UserDTO;
import com.sda.finalproject.dailyjoys.exceptions.IdNotFoundException;
import com.sda.finalproject.dailyjoys.exceptions.UserNotFoundException;
import com.sda.finalproject.dailyjoys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/add")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity addUser(@RequestBody UserDTO user) {

        return new ResponseEntity<>(userService.add(user), HttpStatus.CREATED);

    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity findById(@PathVariable Long id) throws IdNotFoundException {

        return new ResponseEntity<>(userService.findById(id), HttpStatus.OK);

    }

    @GetMapping("/search")  //todo check if user exists
    public ResponseEntity findByUsername(@RequestParam String username) throws UserNotFoundException {
        return new ResponseEntity<>(userService.findByUsername(username), HttpStatus.OK);

    }

    @DeleteMapping("/delete/{id}")
    // todo user has to be admin to delete another user. user can delete their account themselves?
    public ResponseEntity deleteUser(@PathVariable Long id) throws IdNotFoundException {
        return new ResponseEntity<>(userService.delete(id), HttpStatus.OK);
    }

    @PatchMapping("/update/{id}")  // todo check if user is logged in user
    public ResponseEntity updateUser(@PathVariable Long id, @RequestBody UserDTO user) throws IdNotFoundException {
        return new ResponseEntity<>(userService.update(id, user), HttpStatus.OK);
    }


}
