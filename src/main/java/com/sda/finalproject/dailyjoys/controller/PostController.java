package com.sda.finalproject.dailyjoys.controller;


import com.sda.finalproject.dailyjoys.dto.PostDTO;
import com.sda.finalproject.dailyjoys.exceptions.IdNotFoundException;
import com.sda.finalproject.dailyjoys.exceptions.PostNotFoundException;
import com.sda.finalproject.dailyjoys.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("posts")
public class PostController {


    @Autowired
    private PostService postService;

    @PostMapping(value = "/add")
    public ResponseEntity addPost(@RequestBody PostDTO postDTO) {

        return new ResponseEntity<>(postService.add(postDTO), HttpStatus.CREATED);
    }

    @DeleteMapping("/delete/{id}")
    public ResponseEntity deletePost(@PathVariable Long id) throws PostNotFoundException {// todo check if user is logged in user.

        return new ResponseEntity<>(postService.delete(id), HttpStatus.OK);

    }

    @PatchMapping("/update/{id}")
    public ResponseEntity updatePost(@PathVariable Long id, @RequestBody PostDTO post) throws PostNotFoundException {

        return new ResponseEntity<>(postService.update(id, post), HttpStatus.OK);

        // todo check if user is logged in user.
    }

    @GetMapping("/{id}")
    public ResponseEntity findById(@PathVariable Long id) throws PostNotFoundException {
        return new ResponseEntity<>(postService.findById(id), HttpStatus.OK);
    }

    @GetMapping("/show")
    public ResponseEntity showAll() throws IdNotFoundException {

        return new ResponseEntity<>(postService.showAllPosts(), HttpStatus.OK);

        //todo check if user is logged in user - show all posts for that user only.
    }


}
