package com.sda.finalproject.dailyjoys.controller;

import com.sda.finalproject.dailyjoys.exceptions.RequestNotFoundException;
import com.sda.finalproject.dailyjoys.exceptions.UserNotFoundException;
import com.sda.finalproject.dailyjoys.service.RequestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/requests")
public class RequestController {

    @Autowired
    RequestService requestService;

    @PostMapping("/send")
    public ResponseEntity sendFriendRequest(@RequestParam String username) throws UserNotFoundException {

        return new ResponseEntity<>(requestService.sendFriendRequest(username), HttpStatus.CREATED);

    }

    @PutMapping("/{id}/accept")
    public ResponseEntity acceptFriendRequest(@PathVariable Long id) throws RequestNotFoundException {

        return new ResponseEntity<>(requestService.acceptFriendRequest(id), HttpStatus.OK);
    }

    @PutMapping("/{id}/reject")
    public ResponseEntity rejectFriendRequest(@PathVariable Long id) throws RequestNotFoundException {

        return new ResponseEntity<>(requestService.rejectFriendRequest(id), HttpStatus.OK);
    }


}
