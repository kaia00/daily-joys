package com.sda.finalproject.dailyjoys;

import com.sda.finalproject.dailyjoys.dto.UserDTO;
import com.sda.finalproject.dailyjoys.model.User;
import com.sda.finalproject.dailyjoys.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
public class DailyjoysApplication implements CommandLineRunner {

    private final UserService userService;

    @Autowired
    public DailyjoysApplication(UserService userService) {
        this.userService = userService;
    }


    public static void main(String[] args) {
        SpringApplication.run(DailyjoysApplication.class, args);
    }


    @Bean
    public Docket productApi() {
        return new Docket(DocumentationType.SWAGGER_2).select().apis(RequestHandlerSelectors.basePackage("com.sda.finalproject.dailyjoys")).build();
    }

    @Override
    public void run(String... args) throws Exception {

        UserDTO user = new UserDTO();
        user.setUsername("brit");
        user.setFirstName("Brita");
        user.setLastName("Palm");
        user.setAge(10);

        userService.add(user);
    }
}

