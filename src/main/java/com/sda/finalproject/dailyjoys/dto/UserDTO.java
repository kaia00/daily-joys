package com.sda.finalproject.dailyjoys.dto;

import com.sda.finalproject.dailyjoys.model.Friend;
import com.sda.finalproject.dailyjoys.model.Post;
import com.sda.finalproject.dailyjoys.model.Request;
import com.sda.finalproject.dailyjoys.model.User;

import java.util.List;
import java.util.Set;

public class UserDTO {

    private Long id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private int age;
    private String email;
//    private List<Request> sentRequests;
//    private List<Request> receivedRequests;
    private Set<Friend> friends;
    private Set<Post> posts;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

//    public List<Request> getSentRequests() {
//        return sentRequests;
//    }
//
//    public void setSentRequests(List<Request> sentRequests) {
//        this.sentRequests = sentRequests;
//    }
//
//    public List<Request> getReceivedRequests() {
//        return receivedRequests;
//    }
//
//    public void setReceivedRequests(List<Request> receivedRequests) {
//        this.receivedRequests = receivedRequests;
//    }


    public Set<Friend> getFriends() {
        return friends;
    }

    public void setFriends(Set<Friend> friends) {
        this.friends = friends;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }
}
