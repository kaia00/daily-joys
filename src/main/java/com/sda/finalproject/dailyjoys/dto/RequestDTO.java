package com.sda.finalproject.dailyjoys.dto;

import com.sda.finalproject.dailyjoys.model.RequestStatus;
import com.sda.finalproject.dailyjoys.model.User;

import java.time.LocalDateTime;

public class RequestDTO {
    

    private Long id;
//    private User requester;
//    private User receiver;
    private RequestStatus requestStatus;
    private LocalDateTime date;
    private Long receiverId;
    private Long requesterId;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

//    public User getRequester() {
//        return requester;
//    }
//
//    public void setRequester(User requester) {
//        this.requester = requester;
//    }
//
//    public User getReceiver() {
//        return receiver;
//    }
//
//    public void setReceiver(User receiver) {
//        this.receiver = receiver;
//    }

    public RequestStatus getRequestStatus() {
        return requestStatus;
    }

    public void setRequestStatus(RequestStatus requestStatus) {
        this.requestStatus = requestStatus;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    public Long getRequesterId() {
        return requesterId;
    }

    public void setRequesterId(Long requesterId) {
        this.requesterId = requesterId;
    }
}


