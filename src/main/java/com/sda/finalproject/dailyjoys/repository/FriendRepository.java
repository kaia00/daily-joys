package com.sda.finalproject.dailyjoys.repository;

import com.sda.finalproject.dailyjoys.model.Friend;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface FriendRepository extends JpaRepository<Friend, Long> {
}
