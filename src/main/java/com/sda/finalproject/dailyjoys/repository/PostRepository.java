package com.sda.finalproject.dailyjoys.repository;

import com.sda.finalproject.dailyjoys.model.Post;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface PostRepository extends JpaRepository<Post, Long> {

    @Query("select post from Post post where post.userId=:userId")
    List<Post> findPostsByUserId(@Param("userId") Long userId);

}
