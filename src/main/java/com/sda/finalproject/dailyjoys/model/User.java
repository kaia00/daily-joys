package com.sda.finalproject.dailyjoys.model;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@Entity
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String username;
    private String password;
    private String firstName;
    private String lastName;
    private Integer age;
    private String email;

    @OneToMany
    @JoinColumn(name = "friend_id")
    private Set<Friend> friends;

    public Set<Friend> getFriends() {
        return friends;
    }

    public void setFriends(Set<Friend> friends) {
        this.friends = friends;
    }

    //    @OneToMany(mappedBy = "requester")
//    private List<Request> sentRequests;

//    @OneToMany(mappedBy = "receiver")
//    private List<Request> receivedRequests;


    @OneToMany
    @JoinColumn(name = "post_id")
    private Set<Post> posts;

//    @OneToMany
//    @JoinColumn(name = "user_id")
//    private List<Long> friends;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Set<Post> getPosts() {
        return posts;
    }

    public void setPosts(Set<Post> posts) {
        this.posts = posts;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstname) {
        this.firstName = firstname;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
//
//    public List<Request> getSentRequests() {
//        return sentRequests;
//    }
//
//    public void setSentRequests(List<Request> sentRequests) {
//        this.sentRequests = sentRequests;
//    }

//    public List<Request> getReceivedRequests() {
//        return receivedRequests;
//    }
//
//    public void setReceivedRequests(List<Request> receivedRequests) {
//        this.receivedRequests = receivedRequests;
//    }


}
