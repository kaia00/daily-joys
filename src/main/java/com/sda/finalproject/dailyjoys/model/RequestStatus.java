package com.sda.finalproject.dailyjoys.model;

public enum RequestStatus {

    PENDING,
    ACCEPTED,
    REJECTED
}
